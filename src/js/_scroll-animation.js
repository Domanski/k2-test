(function() {
	"use strict";

	var $animationElements = $('.anim-element');
	var $window = $(window);

	//spradza czy przegladarka obsluguje animacje css3
	var detectAnimationCSS3 = function() {
		var animation = false,
			animationstring = 'animation',
			keyframeprefix = '',
			domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
			pfx  = '',
			elm = document.createElement('div');

		if( elm.style.animationName !== undefined ) { animation = true; }

		if( animation === false ) {
			for( var i = 0; i < domPrefixes.length; i++ ) {
				if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
					pfx = domPrefixes[ i ];
					animationstring = pfx + 'Animation';
					keyframeprefix = '-' + pfx.toLowerCase() + '-';
					animation = true;
					break;
				}
			}
		}
		return animation;
	};

	//sprawdza czy element o klasie jest w widoku
	var checkIfInView = function() {
		var windowHeight = $window.height();
		var windowTopPosition = $window.scrollTop();
		var windowBottomPosition = (windowTopPosition + windowHeight);

		$.each($animationElements, function() {
			var $element = $(this);
			var element_height = $element.outerHeight();
			var elementTopPosition = $element.offset().top;
			var elementBottomPosition = (elementTopPosition + element_height);

			//check to see if this current container is within viewport
			if (//(elementBottomPosition >= windowTopPosition) &&
				(elementTopPosition <= windowBottomPosition)) {
				$element.addClass('in-view');
			} else {
				//$element.removeClass('in-view');
			}
		});
	};

	//funkcja inicjujaca
	var init = function() {
		if (detectAnimationCSS3()) {
			$('html').addClass('css3-anim');
			$window.on('scroll resize', checkIfInView);
			$window.trigger('scroll');

			$(".scroll-icon").one("webkitAnimationEnd oanimationend msAnimationEnd animationend", function() {
				$(this).addClass("not-first");
			});
		} else {
			$('html').addClass('no-css3-anim');
		}
	};

	init();
})();