(function() {
	var $sections = [];

	//przy zmianie rozmiaru okna przeliczamy pozycje sekcji
	var recalculateOffset = function() {
		$.each($sections, function(i, $el) {
			var $section = $el.section;
			$el.offset = $section.offset().top;
		})
	};

	//tworzymy tablice sekcji by nie pobierac ich za kazdym razem
	var createSections = function() {
		$('.navigation-step a').each(function() {
			var $section = $($(this).attr('href'));
			$sections.push({
				'section' : $section,
				'offset' : $section.offset().top,
				'link' : $(this)
			});
		});
	};

	//spradzamy pozycje kazdej sekcji
	var checkPosition = function() {
		var scrollTop = $(window).scrollTop();

		$.each($sections, function(i, el) {
			if (el.offset <= scrollTop + $(window).height() / 2) {

				el.link.parent().siblings().removeClass('active');
				el.link.parent().addClass('active');
			} else {
				return false;
			}
		})
	};

	//pokazuje lub ukrywa kropki nawigacji
	var toggleNavigation = function () {
		var scrollTop = $(window).scrollTop();
		if (scrollTop > 200) {
			$('.navigation-step').addClass('show');
		} else {
			$('.navigation-step').removeClass('show');
		}
	};

	//podpina nawigacje
	var bindEvents = function() {
		$(window).on('scroll', function() {
			checkPosition();
			toggleNavigation();
		});

		$(window).on('resize', function() {
			recalculateOffset();
			checkPosition();
		});

		$('.page-navigation a, .navigation-step a').on('click', function(e) {
			var href = $(this).attr('href');
			var $target = $(href);
			if ($target.length) {
				e.preventDefault();
				$('html, body').animate({scrollTop : $target.offset().top}, 2000);
			} else {}
		});

		$('.nav-toggle').on('click', function(e) {
			e.preventDefault();
			$('body').toggleClass('nav-open');
		});
	};

	var addNavigationSteps = function() {
		var $navSteps = $('<div class="navigation-step">'
			+ ' <ul>'
			+ '     <li><a href="#conference">Conference</a></li>'
			+ '     <li><a href="#agenda">Agenda</a></li>'
			+ '     <li><a href="#speakers">Speakers</a></li>'
			+ '     <li><a href="#location">Location</a></li>'
			+ '     <li><a href="#contact">Contact</a></li>'
			+ ' </ul>'
			+ '</div>')
		$('.page-header .page-center').append($navSteps);
	};

	$(function() {
		addNavigationSteps();
		createSections();
		checkPosition();
		toggleNavigation();

		bindEvents();
		checkPosition();
	})
})();