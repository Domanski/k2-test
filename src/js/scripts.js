﻿(function() {
	"use strict";

	//funkcja podpina w sekcji speakers pod przyciski pokazywanie/chowanie danych na temat osob
	var sectionSpeakers = function() {
		$('.section-speakers .close-btn, .section-speakers .open-btn, .section-speakers .speaker-photo img').on('click', function(e) {
			e.preventDefault();
			var $speaker = $(this).closest('.speaker');
			$speaker.toggleClass('open');
			$speaker.siblings().removeClass('open');
		});
	};

	//funkcja podpina w sekcji agendy pokazywanie/chowanie informacji na temat zdarzen
	var sectionAgenda = function() {
		$('.section-agenda .agenda-element-header').on('click', function(e) {
			e.preventDefault();

			var $element = $(this).closest('.agenda-element');
			$element.toggleClass('show');
		});
	};

	//funkcja podpina google mapę - doklaniejsze informacje w pliku js\_googleMap.js
	var sectionGoogleMap = function() {
		var $mapHTMLObj = $('#googleMap');
		var mapConfig = {
			zoom: 10,
			center: new google.maps.LatLng(52.24755, 21.02418),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			draggable: !("ontouchend" in document),
			styles: [
				{ "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" }] }, { "featureType": "landscape", "stylers": [ {"saturation":-100},{"lightness":65},{"visibility":"on"}] }, { "featureType": "poi" , "stylers": [ {"saturation":-100},{"lightness":51},{"visibility":"simplified"}] }, { "featureType": "road.highway", "stylers": [ {"saturation":-100},{"visibility":"simplified"}] }, { "featureType": "road.arterial", "stylers": [ {"saturation":-100},{"lightness":30},{"visibility":"on"}] }, { "featureType": "road.local", "stylers": [ {"saturation":-100},{"lightness":40},{"visibility":"on"}] }, { "featureType": "transit", "stylers": [ {"saturation":-100},{"visibility":"simplified"}]}, { "featureType": "administrative.province", "stylers": [ {"visibility":"off"}] }, { "featureType": "water", "elementType": "labels", "stylers": [ {"visibility":"on"},{"lightness":-25},{"saturation":-100}] }, { "featureType": "water", "elementType": "geometry", "stylers": [ {"hue": "#ffff00"},{"lightness":-25},{"saturation":-97}] }
			]
		};

		googleMap.init($mapHTMLObj, mapConfig);
	};

	//funkcja podpina obsluge wysylki formularza
	var bindSubmitForm = function() {
		$('#contactForm').on('submit', function(e) {
			e.preventDefault();

			var $form = $(this);
			var $btn = $form.find('button:submit');

			$form.find('.error-text').remove();
			$form.find('.error').removeClass('error');
			$btn.addClass('loading');
			$btn.prop('disabled', 1);

			var dataToSend = $form.serializeArray();

			dataToSend = dataToSend.concat(
				$form.find('input:checkbox:not(:checked)').map(
					function() {
						return {"name": this.name, "value": this.value}
					}).get()
			);

			$.ajax({
				url : $form.attr('action'),
				type : $form.attr('method'),
				dataType : 'json',
				data : dataToSend,
				success : function(ret) {
					if (ret.status == 'error') {
						$.each(ret.errors, function(key, value) {
							var el = value;
							var $field = $form.find('input[name='+el.name+'], select[name='+el.name+'], textarea[name='+el.name+']');
							var $row = $field.closest('.row');
							$row.addClass('error');
							$row.append('<div class="error-text">'+el.text+'</div>');
						});
					} else {
						$form.remove();
						$('.message-complete').show();
					}
				},
				complete : function() {
					console.log('sss');
					$btn.removeClass('loading');
					$btn.removeAttr('disabled');
				}
			})
		});
	};

	//wlacza dynamiczna walidacje formularza
	var simpleFormValidation = function() {
		$('#contactForm').find('input[type=text], input[type=email], textarea').on('input change', function() {
			var $this = $(this);
			var $row = $(this).closest('.row');
			if ($this.attr('pattern') !== undefined && $row.hasClass('error')) {
				var reg = new RegExp($this.attr('pattern'), 'gim');
				if (reg.test($this.val())) {
					$row.removeClass("error");
					$row.find('.error-text').remove();
				} else {
					if (!$field.find('.error-error').length) {
						$field.addClass('error');
						var errorMsg = $this.attr('data-error');
						$field.append('<div class="error-error">'+errorMsg+'</div>');
					}
				}
			}
		});

		$('#contactForm').find('input:checkbox').on('click', function() {
			var $this = $(this);
			var $row = $(this).closest('.row');
			if ($this.is(':checked')) {
				$row.removeClass('error');
				$row.find('.error-text').remove();
			}
		});
	};

	//funkcja inicjująca
    var init = function() {
	    $('html').removeClass('no-js');

	    sectionSpeakers();
	    sectionAgenda();
	    sectionGoogleMap();
	    bindSubmitForm();
	    simpleFormValidation();
    };

    init();
})();