//efekt "ripple" (rozchodzacej sie fali) dla przycisków
(function() {
	'use strict';

	var $ripple = $('.btn');

	$ripple.each(function() {
		$(this).append('<span class="ripple"></span>');
	});

	$ripple.on('click', function(e) {
		var $this = $(this);
		var $offset = $this.offset();
		var $circle = $this.find('.ripple');

		var x = e.pageX - $offset.left;
		var y = e.pageY - $offset.top;

		$circle.css({
			top: y + 'px',
			left: x + 'px'
		});

		$this.addClass('is-active');
	});

	$ripple.on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function() {
		$(this).removeClass('is-active');
	});
})();