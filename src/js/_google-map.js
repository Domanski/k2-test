/*
Włącza google mapę dla elementu, który zawiera odpowiednio sformatowane markery. Każdy marker powinien mieć odpowiednie atrybuty:
data-lat, data-lng              : pozycja na google map.
data-img                        : url grafiki markera
data-img-width, data-img-height : rozmiary grafiki markera

------------------------------
Metody
------------------------------
apiCheckboxToggle.init() : odpalenie skryptu

------------------------------------------
Przykładowe użycie:
------------------------------------------
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<div id="googleMap" class="google-map">
    <div class="marker" data-lat="52.262261" data-lng="20.978447" data-img="/images/frontend/googlemap/icon-office.png" data-img-width="100" data-img-height="54">
        lorem ipsum sit dolor
    </div>
</div>

<script>
var latlng = new google.maps.LatLng(52.24755, 21.02418);
var mapOpt = {
    zoom: 10,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
};

$(function(){
    googleMap.init($('#googleMap'), mapOpt);
});
</script>

------------------------------------------
Dodatkowe:
------------------------------------------
Style mapy mozna pobrac ze strony https://snazzymaps.com/
Dodaje się je w opcjach mapy,, przykładowo:

options = {
    zoom: 10,
    center: new google.maps.LatLng(52.24755, 21.02418),
    mapTypeId   : google.maps.MapTypeId.ROADMAP,
    styles: [
        { "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" }] }, { "featureType": "landscape", "stylers": [ {"saturation":-100},{"lightness":65},{"visibility":"on"}] }, { "featureType": "poi" , "stylers": [ {"saturation":-100},{"lightness":51},{"visibility":"simplified"}] }, { "featureType": "road.highway", "stylers": [ {"saturation":-100},{"visibility":"simplified"}] }, { "featureType": "road.arterial", "stylers": [ {"saturation":-100},{"lightness":30},{"visibility":"on"}] }, { "featureType": "road.local", "stylers": [ {"saturation":-100},{"lightness":40},{"visibility":"on"}] }, { "featureType": "transit", "stylers": [ {"saturation":-100},{"visibility":"simplified"}]}, { "featureType": "administrative.province", "stylers": [ {"visibility":"off"}] }, { "featureType": "water", "elementType": "labels", "stylers": [ {"visibility":"on"},{"lightness":-25},{"saturation":-100}] }, { "featureType": "water", "elementType": "geometry", "stylers": [ {"hue": "#ffff00"},{"lightness":-25},{"saturation":-97}] }
    ]
}

 */
var googleMap = (function() {
    var _createMap = function ( $googleMapElement , mapOption ) {
        var $markers = $googleMapElement.find('.marker');
        var options = $.extend({
            zoom: 10,
            center: new google.maps.LatLng(52.24755, 21.02418),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: !("ontouchend" in document),
            scrollwheel: false,
            // style mozna wziasc z https://snazzymaps.com/
            styles: [
                { "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" }] }, { "featureType": "landscape", "stylers": [ {"saturation":-100},{"lightness":65},{"visibility":"on"}] }, { "featureType": "poi" , "stylers": [ {"saturation":-100},{"lightness":51},{"visibility":"simplified"}] }, { "featureType": "road.highway", "stylers": [ {"saturation":-100},{"visibility":"simplified"}] }, { "featureType": "road.arterial", "stylers": [ {"saturation":-100},{"lightness":30},{"visibility":"on"}] }, { "featureType": "road.local", "stylers": [ {"saturation":-100},{"lightness":40},{"visibility":"on"}] }, { "featureType": "transit", "stylers": [ {"saturation":-100},{"visibility":"simplified"}]}, { "featureType": "administrative.province", "stylers": [ {"visibility":"off"}] }, { "featureType": "water", "elementType": "labels", "stylers": [ {"visibility":"on"},{"lightness":-25},{"saturation":-100}] }, { "featureType": "water", "elementType": "geometry", "stylers": [ {"hue": "#ffff00"},{"lightness":-25},{"saturation":-97}] }
            ]
        }, mapOption);

        var map = new google.maps.Map( $googleMapElement[0], options);

        map.markers = [];
        $markers.each(function(){
            _addMarker( $(this), map );
        });

        _centerMap( map, options );
    };

    var _addMarker = function( $marker, map ) {
        var marker = null;
        var lat, lng;

        if (!$marker.is('[data-lat]') || !$marker.is('[data-lng]')) {
            //jezeli nie ma ustawionych wspolrzednych, ustawiam na Noma
            lat = 52.19787;
            lng = 20.98206;
        } else {
            lat = Number($marker.attr('data-lat'));
            lng = Number($marker.attr('data-lng'));
        }

        var latlng = new google.maps.LatLng( lat, lng );

        if ($marker.is('[data-img]') && $marker.is('[data-img-width]') && $marker.is('[data-img-height]')) {

            var image = new google.maps.MarkerImage(
                $marker.attr('data-img'),
                new google.maps.Size( parseInt($marker.attr('data-img-width')), parseInt($marker.attr('data-img-height')) ),
                new google.maps.Point(0, 0),
                new google.maps.Point( parseInt(parseInt($marker.attr('data-img-width'))/2), parseInt($marker.attr('data-img-height')) )
            );

            marker = new google.maps.Marker({
                position    : latlng,
                map         : map,
                icon        : image
            });

        } else {

            marker = new google.maps.Marker({
                position    : latlng,
                map         : map
            });

        }

        marker.url = 'http://maps.google.com/maps?q=loc:'+lat+','+lng;

        if( !$marker.is('[data-no-click]') ) {
            if( $marker.is('[data-show-tooltip]') ) {
                var infowindow = new google.maps.InfoWindow({
                    content     : $marker.html()
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open( map, marker );
                });
            } else {
                google.maps.event.addListener(marker, 'click', function() {
                    window.location.href = marker.url;
                });
            }
        }

        map.markers.push( marker );
    };

    var _centerMap = function ( map, mapOption ) {
        var bounds = new google.maps.LatLngBounds();

        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });

        if( map.markers.length <= 1 ) {
            map.setCenter( bounds.getCenter() );
            if (typeof mapOption.zoom !== undefined ) {
                map.setZoom( mapOption.zoom );
            } else {
                map.setZoom( 8 );
            }
        } else {
            map.fitBounds( bounds );
        }
    };

    var _init = function($mapHTMLObj, mapOption) {
        if (!$mapHTMLObj.length) {
            console.log('Error: element mapy w html nie zostal znaleziony');
            return;
        }

        if (google == undefined) {
            console.log('Error: Przed wywolaniem google map dodaj skrypt google maps do strony (http://maps.google.com/maps/api/js?sensor=false).')
            return;
        }

        _createMap( $mapHTMLObj, mapOption );
    };

    return {
        init : _init
    }
})();