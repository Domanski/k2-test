var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
//var rename = require('gulp-rename'); //https://github.com/hparra/gulp-rename/issues/37
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var notifier = require('node-notifier');
var cssmin = require('gulp-cssmin');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var color = require('gulp-color');


//============================================
//Compile Sass
//============================================
    gulp.task('sass:prod', function() {
	    return gulp.src('src/scss/style.scss')
		    .pipe(sourcemaps.init())
            .pipe(sass({ outputStyle : 'compressed' }))
            .pipe(autoprefixer({browsers: ["> 1%"]}))
		    .pipe(cleanCSS())
		    .pipe(sourcemaps.write('.'))
		    .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.reload({stream:true}));
    });

	gulp.task('sass:dev', function() {
		return gulp.src('src/scss/style.scss')
			.pipe(sourcemaps.init())
			.pipe(sass({ outputStyle : 'expanded' }))
			.pipe(autoprefixer({browsers: ["> 1%"]}))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('dist/css'))

			.pipe(browserSync.reload({stream:true}));

	});


//============================================
//Compile JS
//============================================
    // Lint Task -----------------------------
    gulp.task('js-lint', function() {
        return gulp.src('src/js/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
    });

    // Concatenate JS -----------------------------
    gulp.task('js:dev', function() {
        return gulp.src('src/js/*.js')
	        .pipe(sourcemaps.init())
            .pipe(concat('scripts.js'))
	        .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('dist/js'))
	        .pipe(browserSync.reload({stream:true}));
    });

    // Minify JS --------------------
    gulp.task('js:prod', function() {
        return gulp.src('src/js/*.js')
	        .pipe(sourcemaps.init())
	        .pipe(concat('scripts.js'))
	        .pipe(uglify())
	        .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('dist/js'))
	        .pipe(browserSync.reload({stream:true}));
    });


//============================================
//Notify tasks
//============================================
	gulp.task('notify-js', function() {
		notifier.notify({
			'title': 'Rozpoczynamy pracę milordzie.',
			'message': 'Połączyłem skrypty do pliku dist/js/scripts.min.js'
		});
	});

	gulp.task('notify-sass', function() {
		notifier.notify({
			'title': 'Zapisałem SCSS',
			'message': 'Style zostały zapisane w pliku dist/css/style.min.css'
		});
	});


//============================================
//watch tasks
//============================================
	gulp.task('watch:prod', function() {
		gulp.watch('src/js/*.js', ['js-lint', 'js:prod', 'notify-js']);
		gulp.watch('src/scss/**/*.scss', ['sass:prod', 'notify-sass']);
	});

	gulp.task('watch:dev', function() {
		gulp.watch('src/js/*.js', ['js-lint', 'js:dev', 'notify-js']);
		gulp.watch('src/scss/**/*.scss', ['sass:dev', 'notify-sass']);
	});


//============================================
//Browser sync
//============================================
	gulp.task('browser-sync', function() {
		browserSync.init({
			server: {
				baseDir: "./dist"
			},
			notify: false,
			ghostMode: {
				clicks: true,
				location: true,
				forms: true,
				scroll: false
			}
		});
	});


//============================================
//Global tasks
//============================================
    // Default Task    
    gulp.task('dev', function() {
	    gulp.start('sass:dev', 'js-lint', 'js:dev', 'watch:dev', 'browser-sync');
	    console.log('-------------------------------------------');
	    console.log('Rozpoczynamy pracę milordzie (DEV)');
	    console.log('-------------------------------------------');
    });

    gulp.task('prod', function() {
	    gulp.start('sass:prod', 'js:prod', 'watch:prod', 'browser-sync');
	    console.log(color('-------------------------------------------', 'YELLOW'));
	    console.log(color('Rozpoczynamy pracę milordzie (PROD)', 'YELLOW'));
	    console.log(color('-------------------------------------------', 'YELLOW'));
    });

    gulp.task('default', ['dev']);


