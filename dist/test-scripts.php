<?php
/*
 * Skrypt do testowania dzialania formularza wysylkowego.
 */

$mailToSend = 'xxxxx@wp.pl';

sleep(2);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //honejpod antyspam metod
    //dla wiekszej skutecznosci to pole nazwalem "age"
    if (!isset($_POST['age']) && empty($_POST['age'])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $message = $_POST['email'];
        $regulation = $_POST['regulation'];

        $status = 'ok';
        $errors = [];
        $return = [];

        if (empty($name)) {
            array_push($errors, array('name' => 'name', 'text' => 'Proszę podać imię'));
            $status = 'error';
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errors, array('name' => 'email', 'text' => 'Proszę podać prawidłowy email'));
            $status = 'error';
        }
        if (empty($subject)) {
            array_push($errors, array('name' => 'subject', 'text' => 'Proszę podać temat'));
            $status = 'error';
        }
        if (empty($message)) {
            array_push($errors, array('name' => 'message', 'text' => 'Proszę podać wiadomość'));
            $status = 'error';
        }
        if (empty($message)) {
            array_push($errors, array('name' => 'regulation', 'text' => 'Proszę zaakceptować regulamin'));
            $status = 'error';
        }

        $return = [
            'status' => $status
        ];

        if ($status == 'error') {

            $return['errors'] = $errors;

        } else {

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8'. "\r\n";
            $headers .= 'From: '.$email."\r\n";
            $headers .= 'Reply-to: '.$email;

            $message  = "
                <html>
                    <head>
                        <meta charset=\"utf-8\">
                    </head>
                    <style type='text/css'>
                        body {font-family:sans-serif; color:#222; padding:20px;}
                        div {margin-bottom:10px;}
                        .msg-title {margin-top:30px;}
                    </style>
                    <body>
                        <div>Imię: <a href=\"mailto:$name\">$name</a></div>
                        <div>Email: <strong>$email</strong></div>
                        <div>Temat wiadomości: <strong>$subject</strong></div>                       
                        <div class='msg-title'><strong>Wiadomość:</strong></div>
                        <div>$message</div>
                    </body>
                </html>";
            @mail($mailToSend, 'Wiadomość ze strony Konferencji - ' + date("d-m-Y"), $message, $headers);
        }



        header('Content-Type: application/json');
        echo json_encode($return);
    }
}