﻿Strona konferencji
==================

Struktura katalogów:
--------------------
- gulpfile.js
- package.js
- src
  - js - pliki js
  - scss - style scss
- dist - tresc gotowa
  - js - skrypty zminimalizowane
  - css - style zminimalizowane
  - images - grafiki
  - fonts - fonty dolaczone do strony
  - index.html - glowny plik
       
       
Instalacja projektu (dla frontendowca)
----------------------------------------
Strona została pocięta z wykoszystaniem JS/CSS3/HTML/PHP.  
Formularz kontaktowy do działania wymaga php.

Do postawienia projektu potrzebne są:  
node (https://nodejs.org/en/download/)  
gulp (https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)  

Następnie musimy zainstalować modyły node (```$ npm install```).  

Od tej chwili będziemy mieli dostęp do tasków:  
``$ gulp browser-sync`` - podglad projektu w przeglądarce  
``$ gulp dev`` - kompilacja projektu w wersji developerskiej (bez minimalizacji)
``$ gulp prod`` - kompilacja projektu w wersji produkcyjnej (z minimalizacją) + kilka innych tasków, które można zobaczyć w pliku gulpfile.js :)


Działanie formularza kontaktowego
---------------------------------
Formularz wskazuje na przykładowy plik /dist/test-scripts.php  
Gdy zostanie wysłany, sprawdzam w tym skrypcie wartości pól. Jeżeli wszystko jest ok ustawiam w zwracanym json "status" na ok i wysyłam maila (jest to oczywiście przykład i przydało by się tutaj zaimplementować coś bardziej celnego).  
Jeżeli pola są błędnie wypełnione, wtedy tworzona jest tablica "errors", która zawiera w sobie pary {"nazwa_pola","tekst błędu"}.  
Js czeka na odpowiedź. Jeżeli w odpowiedzi dostanie "errors", wtedy robi pętlę i dla wskazanych pól dodaje klasę .error i wyświetla
odpowiedni komunikat błędu. Dodatkowo stworzona jest dynamiczna walidacja. Po pokazaniu błędu gdy użytkownik zacznie coś wpisywać
trzeba ukryć pobrany z serwera błąd.


Działanie animacji na stronie
-----------------------------
Animacja na stronie praktycznie w całości zrobiona jest za pomocą CSS3. Skrypt sprawdza jedynie czy dany element znalazł się już na stronie (src/js/_scroll-animation.js:45).   
Jeżeli tak, nadawana jest mu klasa .in-view, a całość animacji wykonywana jest za pomocą CSS3 w pliku **/src/scss/partials/_animation-on-scroll.scss**

Strona domyślnie jest pocięta bez animacji pojawiających się elementów. Skrypt sprawdza, czy przeglądarka obsługuje animację CSS3
(src/js/_scroll-animation.js:56). Jeżeli tak jest, wtedy nadawana jest dla dokumentu klasa .css3-anim.  
Dzięki tej klasie nadawane są animacje z pliku **/src/scss/partials/_animation-on-scroll.scss**


Dodatkowe rzeczy
----------------
Dla google mapy wykorzystałem autorski skrypt, który ułatwia implementację takiej mapy. Robi on pętlę po znacznikach  
które są umieszczone w mapie i obsługując ich parametry (patrz opis na poczatku pliku /src/js/_google-map.js) dodaje klikalne (lub nie klikalne)  
ikony na mapie. Można to spokojnie zrobić bez tego skryptu...  

Przy skrolowaniu z prawej strony pokazuję kropki, które mogą (ale nie muszą) ułatwić nawigację, oraz wskazują aktualną sekcję.   
Skrypt odpowiedzialny za to umieściłem w pliku src/js/_navigation.js.

Skrypty zostały napisane w jQuery. Jako że cała animacja jest zrobiona za pomocą CSS3, bardzo łatwo można je przepisać na czysty JS.
